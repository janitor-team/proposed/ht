ht (2.1.0+repack1-5) unstable; urgency=medium

  * [5b3813d] Add simple autopkgtest

 -- Anton Gladky <gladk@debian.org>  Sun, 03 May 2020 19:26:32 +0200

ht (2.1.0+repack1-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol

  [ Anton Gladky ]
  * [7b6de12] Set compat-level to 13
  * [7708a4e] Set Standards-Version to 4.5.0
  * [8fb3a2a] Add Rules-Requires-Root
  * [bdb9c8c] Set field Upstream-Name in debian/copyright.
  * [4f1a170] Fix FTBFS with GCC-10. (Closes: #957340)
  * [e8a712c] Update d/watch

 -- Anton Gladky <gladk@debian.org>  Sun, 03 May 2020 19:14:19 +0200

ht (2.1.0+repack1-3) unstable; urgency=medium

  [ Stephen Kitt ]
  * [0b99929] Force unambiguous signed abs() call, to build with GCC 7.
              (Closes: #853444)

  [ Anton Gladky ]
  * [b20cc09] Bump Standards-Version: 4.0.0. No changes
  * [7ee09b3] Switch to compat level 10

 -- Anton Gladky <gladk@debian.org>  Fri, 11 Aug 2017 22:20:54 +0200

ht (2.1.0+repack1-2) unstable; urgency=high

  * [107d7de] Set the minimal libiberty version to 20161017 in BD.
              CVE-2016-6131

 -- Anton Gladky <gladk@debian.org>  Mon, 17 Oct 2016 20:19:33 +0200

ht (2.1.0+repack1-1) unstable; urgency=critical

  * [384c0d4] New upstream version 2.1.0+repack1
  * [38ec499] Use packaged version of libiberty instead of embedded.
              (Closes: #840358)
              CVE-2016-4487 CVE-2016-4488 CVE-2016-4489 CVE-2016-4490
              CVE-2016-4491 CVE-2016-4492 CVE-2016-4493 CVE-2016-2226
              CVE-2016-6131
  * [b2c2049] Remove info-file.
  * [19ad75b] Set flags in Makefile to link packaged libiberty.
  * [60f8762] Apply cme fix-dpkg.

 -- Anton Gladky <gladk@debian.org>  Thu, 13 Oct 2016 23:24:45 +0200

ht (2.1.0-1) unstable; urgency=medium

  * [dc9c185] Update manpage. (Closes: #778823)
  * [42cd539] Apply cme fix dpkg-control.
  * [c1eaa42] Update d/copyright.
  * [ef41c3b] Remove __TIMEDATE__ preppreprocessors to get a
              reproducible build.

 -- Anton Gladky <gladk@debian.org>  Sun, 21 Jun 2015 15:05:51 +0200

ht (2.1.0-1~exp1) experimental; urgency=medium

  * [14367fd] Update d/watch.
  * [112ef52] Update d/copyright.
  * [601637c] Imported Upstream version 2.1.0. (Closes: #773308)
  * [be33f94] Refresh patch.
  * [d0ff76c] Set Standards-Version: 3.9.6. No changes.
  * [8975ed2] Fix format-not-a-string issue.

 -- Anton Gladky <gladk@debian.org>  Wed, 18 Feb 2015 21:32:50 +0100

ht (2.0.22-2) unstable; urgency=low

  * [75cb86c] Use 3.0 (quilt) format.
  * [ced07b9] Remove README.source (obsolete).
  * [05cbf64] Ignore quilt dir.
  * [650726a] Unapply patches after build.
  * [a065931] Refresh patch.
  * [a5b4a8c] Simplify/shorten debian/rules.
  * [b0c028a] Add info and manpages files.
  * [8e927fb] Use DEP-5 for copyright-file.
  * [b13d5fe] Set myself as maintainer. (Closes: #728261)
  * [e5cfafd] Migrate VCS to git.
  * [6dc628c] Use wrap-and-sort.

 -- Anton Gladky <gladk@debian.org>  Fri, 01 Nov 2013 21:09:15 +0100

ht (2.0.22-1) unstable; urgency=low

  * Orphan package
  * New upstream release
    * Drop patches/11_makeinfo_syntax.diff (applied upstream)
  * Bump standards to 3.9.4 (no changes needed)
  * Update config.{guess,sub} during build; add build-dependency on
    autotools-dev (Closes: #727897)
  * use dh_prep instead of dh_clean

 -- Alexander Reichle-Schmehl <tolimar@debian.org>  Wed, 30 Oct 2013 03:07:22 +0100

ht (2.0.20-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix doc/ht.texi for more recent makeinfo (Closes: #712348)

 -- Helmut Grohne <helmut@subdivi.de>  Mon, 08 Jul 2013 08:41:47 +0200

ht (2.0.20-2) unstable; urgency=medium

  * debian/rules: Make sure configure-stamp is called, even when only
    build-arch target is called (Closes: #675844)
  * Set urgency to medium due to rc bug fix

 -- Alexander Reichle-Schmehl <tolimar@debian.org>  Sun, 03 Jun 2012 18:37:05 +0200

ht (2.0.20-1) unstable; urgency=low

  * New upstream release
  * Fix spelling mistake in package description found by lintian:
    fix "allows one to" phrases
  * Add recommended build-arch and build-indep to debian/rules
  * Bump standards to 3.9.3 (no changes needed)
  * Fix build-dependency on libncurses5-dev and libncursesw5-dev

 -- Alexander Reichle-Schmehl <tolimar@debian.org>  Wed, 23 May 2012 15:11:15 +0200

ht (2.0.18-1) unstable; urgency=low

  * new upstream version
  * Bumped standards to 3.8.3
    - no changes needed
  * Add depends on ${misc:Depends} as recommended by lintian and debhelper

 -- Alexander Reichle-Schmehl <tolimar@debian.org>  Mon, 18 Jan 2010 14:53:28 +0100

ht (2.0.17-1) unstable; urgency=low

  * New upstream release
  * Fixing typo in package description reported by Bernd Zeimetz.  Thanks!
  * Pumping standards version to 3.8.1
    - no changes needed

 -- Alexander Reichle-Schmehl <tolimar@debian.org>  Sun, 03 May 2009 19:19:53 +0200

ht (2.0.16-1) unstable; urgency=low

  * The "Greeting from Samoa" upload
  * New upstram release

 -- Alexander Reichle-Schmehl <tolimar@debian.org>  Sun, 15 Feb 2009 05:47:31 -1100

ht (2.0.15-1) experimental; urgency=low

  * New upstram release
  * Updating debian/control for changed maintainer name
  * Updated standards to 3.8.0 (no changes needed)
    * Use /usr/share/quilt/quilt.make to provide patch and unpatch targets
    * Provide debian/README.source
  * Fixed info-page (was incomplete addopted to the change to hte)
    (Changed via debian/patches/10_fix_info_files.diff)
    (Closes: #508665)

 -- Alexander Reichle-Schmehl <tolimar@debian.org>  Sun, 18 Jan 2009 17:35:35 +0100

ht (2.0.14-1) unstable; urgency=low

  [ Alexander Schmehl ]
  * new upstream release

  [ Luk Claes ]
  * Remove myself from Uploaders.

 -- Alexander Schmehl <tolimar@debian.org>  Wed, 28 May 2008 20:55:00 +0200

ht (2.0.12-2) unstable; urgency=low

  * Removed symlinks "ht"; added hint to package description
    (Closes: #471841)

 -- Alexander Schmehl <tolimar@debian.org>  Thu, 20 Mar 2008 19:27:06 +0100

ht (2.0.12-1) unstable; urgency=low

  * New upstream release
  * adjusted Vcs-Browser to link to the nicer interface
  * Adding symlinks of the package name to the actualy binary

 -- Alexander Schmehl <tolimar@debian.org>  Wed, 19 Mar 2008 21:24:08 +0100

ht (2.0.11-1) unstable; urgency=low

  * New upstream release
  * Added Vcs-Svn and Vcs-Browser to debian/control
  * Moved Homepage to proper field in debian/control
  * bumped standards Version to 3.7.3 (no change needed)
  * Upgrading menu file for new menu policy
  * fixed watchfile (Closes: #449825)
  * debian/changelog updated

 -- Alexander Schmehl <tolimar@debian.org>  Thu, 21 Feb 2008 02:57:18 +0100

ht (2.0.8-2) unstable; urgency=low

  * Remove me from Uploaders.

 -- Luk Claes <luk@debian.org>  Wed, 23 Jan 2008 23:03:13 +0000

ht (2.0.8-1) unstable; urgency=low

  * New Upstream Release
  * Adding liblzo2-dev to build depends; apparently ht can use that, too
    * Thanks Steinar Gunderson for spotting the problem
  * Removing "Warning! Software is still beta!" paragraph from package
    description; it officially stable.
  * correcting debian/copyright:  It's GPL-2 only, no "any later" clause
  * Rebuilding patched info files:
    * adding texinfo to build-depends
    * adding quilt to build-depends
    * adding quilt push and pop calls to debian/rules
    * rebuild info files in debian/rules
    * make lintian happy :)

 -- Alexander Schmehl <tolimar@debian.org>  Sun, 29 Jul 2007 01:16:56 +0200

ht (2.0.3-2) unstable; urgency=low

  * Upload to unstable.

 -- Luk Claes <luk@debian.org>  Sat, 21 Apr 2007 15:45:03 +0200

ht (2.0.3-1) experimental; urgency=low

  * New upstream release

 -- Luk Claes <luk@debian.org>  Thu, 19 Apr 2007 21:18:04 +0200

ht (2.0~beta-1) experimental; urgency=low

  * New upstream.

 -- Luk Claes <luk@debian.org>  Tue, 21 Nov 2006 19:32:58 +0100

ht (0.9.1-4) unstable; urgency=low

  * New maintainer and co-maintainer (Closes: #312078).

 -- Luk Claes <luk@debian.org>  Tue, 16 May 2006 00:12:34 +0200

ht (0.9.1-3) unstable; urgency=low

  * Fix FTBFS with G++ 4.1: extra qualifications; thanks to Martin Michlmayr
    (Closes: #356156)
  * Drop preinst / NEWS.Debian that were only needed for upgrades to Sarge
  * Upgrade debhelper compatibility level

 -- Florian Ernst <florian@debian.org>  Mon, 13 Mar 2006 12:35:39 +0100

ht (0.9.1-2) unstable; urgency=low

  * Include patch generated by Andreas Jochens to build on 64bit archs using
    gcc-4.0 (Closes: #293706)

 -- Florian Ernst <florian@debian.org>  Tue,  9 Aug 2005 16:32:56 +0200

ht (0.9.1-1) unstable; urgency=low

  * New upstream release, merging the unstable and experimental branches
    + builds fine with gcc-4.0 (Closes: #293706)
  * debian/control: Standards-Version 3.6.2, no changes required

 -- Florian Ernst <florian@debian.org>  Tue,  9 Aug 2005 13:32:56 +0200

ht (0.8.0-3) unstable; urgency=high

  * Urgency high due to security fix
  * Added two integer overflow precautions in the ELF parser [htelfshs.cc,
    CAN-2005-1545], thanks to Martin 'Joey' Schulze and the Security Team!

 -- Florian Ernst <florian@debian.org>  Sun, 05 Jun 2005 01:52:24 +0200

ht (0.9.0-1) experimental; urgency=low

  * New upstream release
    + contains patches introduced in 0.8.0-2, backing them out now
  * debian/rules:
    + install additional upstream info
    + move docs for dh_installdocs into debian/docs
  * debian/control:
    + improve long description to make ht easier to be found
  * Thanks to Reimar Doeffinger for many hints!

 -- Florian Ernst <florian@debian.org>  Fri,  3 Jun 2005 22:31:38 +0200

ht (0.8.0-2) unstable; urgency=high

  * Urgency high due to security fix
  * Security fix pulled from upstream CVS (Closes: #308587)
    + fix an integer overflow in the ELF segment parsing
      (cplus-dem.c, htanaly.cc, htcoff.cc, htelf.cc, htpef.cc, htpeimp.cc)
    + fix some buffer overflows in the PE parser
      (htperes.cc)
    + this is also Gentoo GLSA 200505-08
    Thanks a lot to Moritz Muehlenhoff for the report!
  * debian/control: added upstream homepage to long description

 -- Florian Ernst <florian@debian.org>  Wed, 11 May 2005 20:02:24 +0200

ht (0.8.0-1) unstable; urgency=low

  * New maintainer (Closes: #300592)
  * New upstream release (Closes: #292680)
    + supposedly compiles with gcc-3.4 (Closes: #269922)
  * debian/control:
    + removed B-D on autotools-dev; not needed anymore
    + added B-D on libx11-dev to enable X11 textmode support
    + Standards-Version 3.6.1
    + updated description
  * debian/copyright:
    + updated upstream and maintainer info
    + extended copyright and license info
  * debian/hte.1: use "hte" in manpage
  * debian/menu: quote entries
  * debian/prerm: removed, not needed
  * debian/preinst: added to facialiate updates from previous wrong uses
    of the alternates system
  * debian/rules:
    + DH_COMPAT > debian/compat
    + redo, following best practices
  * debian/watch: added
  * debian/NEWS.Debian: added, explaining binary renaming
  * Acknowledge NMUs, thanks to Roland Mas and Sebastien Bacher
    (Closes: #168654, #186672, #201394, #199293)

 -- Florian Ernst <florian@debian.org>  Thu,  7 Apr 2005 15:47:49 +0200

ht (0.7.3-0.1) unstable; urgency=low

  * NMU (required to fix a RC bug)
  * New upstream release (Closes: #199293).
  * Fixed FTBFS on ia64 (Closes: #186672).
  * Fixed menu entry (Closes: #201394).
  * Replaced "Author(s)" with "Authors".
  * Updated standards versions to 3.6.0.

 -- Sebastien Bacher <seb128@debian.org>  Sun, 20 Jul 2003 02:39:38 +0200

ht (0.7.0-1.1) unstable; urgency=low

  * Non-maintainer upload during bug-squashing party.
  * Updated config.guess and config.sub (closes: #168654).

 -- Roland Mas <lolando@debian.org>  Sat, 15 Mar 2003 20:38:25 +0100

ht (0.7.0-1) unstable; urgency=low

  * New upstream version.

 -- Lenart Janos <ocsi@debian.org>  Wed,  6 Nov 2002 01:19:50 +0100

ht (0.6.0b-1) unstable; urgency=medium

  * New upstream version.
  * Renamed binary. The new name is "hte".          (closes: Bug#128188)
  * Shortly: s/dprintf/DPRINTF :)                   (closes: Bug#129132)

 -- Lenart Janos <ocsi@debian.org>  Mon, 17 Jun 2002 12:58:49 +0200

ht (0.5.0-1) unstable; urgency=low

  * New upstream version.
  * Added flex to Build-Depends.        (closes: Bug#119991, Bug#116575)

 -- Lenart Janos <ocsi@debian.org>  Thu, 22 Nov 2001 16:43:31 +0100

ht (0.4.5-1) unstable; urgency=low

  * New upstream version.
  * Some rather strange things removed from the diff.
  * Emacs-thingy removed from this changelog.

 -- Lenart Janos <ocsi@debian.org>  Sun, 21 Oct 2001 00:35:52 +0200

ht (0.4.4d-1) unstable; urgency=low

  * New upstream version.
  * Now it compiles with gcc 3.0 and on hppa.       (closes: Bug#104696)
  * Back to the alternatives system.

 -- Lenart Janos <ocsi@debian.org>  Fri, 17 Aug 2001 19:33:28 +0200

ht (0.4.4c-3) unstable; urgency=low

  * Manpage removed from the alternatives system.

 -- Lenart Janos <ocsi@debian.org>  Wed, 20 Jun 2001 20:59:57 +0200

ht (0.4.4c-2) unstable; urgency=low

  * Manpage /usr/share/man/man1/ht.1.gz is now using the alternatives
    system to avoid overlap with another package.   (closes: Bug#101200)
  * Binary moved from /bin to /usr/bin.             (closes: Bug#101412)

 -- Lenart Janos <ocsi@debian.org>  Tue, 19 Jun 2001 03:57:38 +0200

ht (0.4.4c-1) unstable; urgency=low

  * Initial Release.

 -- Lenart Janos <ocsi@debian.org>  Thu, 14 Jun 2001 00:56:58 +0200
